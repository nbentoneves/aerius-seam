package org.itsector.aeriscaelum.integration;

import org.itsector.aeriscaelum.domain.Localidade;
import org.jboss.seam.mock.SeamTest;
import org.junit.Test;
import static org.testng.Assert.*;

public class RegistarTrechoTest extends SeamTest {
	
	@Test
	public void testLoadTrechos(){
		
		new ComponentTest() {
			
			@Override
			protected void testComponents() throws Exception {
				setValue("#{trechoHandler.trecho.origem}", Localidade.SAO_PAULO);
				setValue("#{trechoHandler.trecho.destino}", Localidade.RIO_DE_JANEIRO);
				
				assertNull(invokeMethod("#{trechoHandler.salvar}"));
			}
		};
		
	}
	
}
