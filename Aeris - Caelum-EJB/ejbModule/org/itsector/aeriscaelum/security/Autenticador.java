package org.itsector.aeriscaelum.security;

import java.util.HashMap;
import java.util.Map;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;
import org.jboss.seam.security.Credentials;
import org.jboss.seam.security.Identity;

@Name("autenticador")
@Scope(ScopeType.APPLICATION)
public class Autenticador {

	@Logger
	private Log log;

	@In
	private Identity identity;

	private final Map<String, String> roles;

	@In
	private Credentials credentials;

	public Autenticador() {
		this.roles = new HashMap<String, String>();
		this.roles.put("aeris", "empresa");
		this.roles.put("cliente", "comprador");
	}

	public boolean autenticar() {
		log.info("Autenticando #0", credentials.getUsername());
		if ("aeris".equals(this.credentials.getUsername())
				|| "cliente".equals(this.credentials.getUsername())) {
			identity.addRole(this.roles.get(this.credentials.getUsername()));
			return true;
		}
		return false;
	}

	public boolean isEmpresa() {
		return identity.hasRole("empresa");
	}

	public boolean isComprador() {
		return identity.hasRole("comprador");
	}
	
}
