package org.itsector.aeriscaelum.session;

import org.itsector.aeriscaelum.domain.Localidade;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

@Name("localidadesHandler")
public class LocalidadesHandler {

	/*
	 * @Factory(value = "localidades", scope = ScopeType.APPLICATION) public
	 * Map<String, Localidade> getLocalidades(){
	 * 
	 * System.out.println("Iniciando Localidades"); Map<String, Localidade>
	 * opcoes = new HashMap<String, Localidade>(); for(Localidade l:
	 * Localidade.values()){ opcoes.put(l.getNome(), l); } return opcoes; }
	 */
	
	@Logger
	private Log log;

	@Factory(value = "localidades", scope = ScopeType.APPLICATION)
	public Localidade[] getLocalidades() {

		Localidade[] localidades = Localidade.values();
		for(Localidade localidade: localidades){
			log.info("Carregar localidade: #0",localidade);
		}
		return localidades;
	}

}
