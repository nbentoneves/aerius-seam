package org.itsector.aeriscaelum.session;

import java.util.List;

import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;

import org.itsector.aeriscaelum.domain.Trecho;
import org.itsector.aeriscaelum.exception.DAOException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.core.Events;
import org.jboss.seam.log.Log;


@Name("trechoHandler")
@Scope(ScopeType.EVENT)
// The some Request Scope
public class TrechoHandler {

	@In
	private EntityManager entityManager;

	@In
	private Events events;
	
	@DataModel
	private List<Trecho> trechos;

	@DataModelSelection
	private Trecho trechoSelecionado;
	
	@Logger
	private Log log;
	
	private Trecho trecho = new Trecho();

	/**
	 * EVENTS
	 */
	public void editar(){
		log.info("Editar trecho #0", trechoSelecionado);
		this.trecho = trechoSelecionado;
	}
	
	public String remover(){
		log.info("Apagar trecho #0", trechoSelecionado);
		this.entityManager.remove(trechoSelecionado);
		throw new DAOException();
		//return "/trechos.xhtml"; //Redirect para caso seja necess�rio atualizar a p�gina
	}
	
	public void salvar(final ActionEvent event) {
		log.info("Guardar trecho!");
		this.trecho = this.entityManager.merge(this.trecho);
		this.events.raiseEvent("novoTrecho", this.trecho);
		log.info("Guardar trecho #0", this.trecho);
		this.trecho = new Trecho();
	}

	/**
	 * Getters
	 */
	public Trecho getTrecho() {
		return this.trecho;
	}
	
	@Factory("trechos")
	public void getTrechos(){
		System.out.println("Buscando trechos do bando de dados");
		this.trechos = this.entityManager.createQuery("from Trecho").getResultList();
	}

}
