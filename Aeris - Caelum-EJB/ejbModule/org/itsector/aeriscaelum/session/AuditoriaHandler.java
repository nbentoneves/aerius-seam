package org.itsector.aeriscaelum.session;

import org.itsector.aeriscaelum.domain.Trecho;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.log.Log;

@Name("auditoriaHandler")
@Scope(ScopeType.EVENT)
public class AuditoriaHandler {

	@Logger
	private Log log;
	
	@Observer("novoTrecho")
	public void registraQueTrechoFoiAdicionado(Trecho trecho){
		log.info("Novo Trecho registado! #0", trecho);
	}
	
}
