package org.itsector.aeriscaelum.session;

import org.itsector.aeriscaelum.domain.Trecho;
import org.itsector.aeriscaelum.domain.Voo;

public interface VooHandler {

	String manipularVoos(Trecho trecho);

	String salvarVoo();

	void editarVoo();

	String apagarVoo();

	void destruirEJB();

	Voo getVoo();
	
	Trecho getTrechoSelecionado();

	void iniciarVoos();
}
