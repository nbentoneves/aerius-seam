package org.itsector.aeriscaelum.session;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.hibernate.Session;
import org.itsector.aeriscaelum.domain.Trecho;
import org.itsector.aeriscaelum.domain.Voo;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Begin;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.security.Restrict;
import org.jboss.seam.log.Log;

@Stateful
@Local(value = VooHandler.class)
@Name("vooHandler")
@Scope(ScopeType.CONVERSATION)
public class VooHandlerBeanImpl implements VooHandler {

	@Logger
	private Log log;

	//@PersistenceContext(type = PersistenceContextType.EXTENDED)
	//Utiliza o entityManager controlado pelo container do EJB
	@In
	private EntityManager entityManager;

	@In(required = false)
	@Out(required = false)
	private Voo voo;

	@DataModel
	private List<Voo> voos;

	@DataModelSelection
	private Voo vooSelecionado;

	private Trecho trechoSelecionado;

	/**
	 * Routers
	 */
	@Begin
	@Restrict("#{s:hasRole('empresa')}")
	public String manipularVoos(final Trecho trecho) {
		this.trechoSelecionado = trecho;
		this.entityManager.merge(trecho);
		log.info("Entity Manager = #0", this.entityManager);
		log.info("Trecho Selecionado: #0", this.trechoSelecionado);
		return "/voos.xhtml";
	}

	/**
	 * Persistence
	 */
	public String salvarVoo() {
		log.info("Entity Manager = #0", this.entityManager);
		this.voo.setTrecho(this.trechoSelecionado);
		if (this.voo.getId() != null) {
			List<Voo> voosAux = this.trechoSelecionado.getVoos();
			for (int i = 0; i < voosAux.size(); i++) {
				if (voosAux.get(i).getId().equals(this.voo.getId())) {
					this.voo = this.entityManager.merge(this.voo);
					this.trechoSelecionado.getVoos().set(i, this.voo);
					break;
				}
			}
		} else {
			this.voo = this.entityManager.merge(this.voo);
			this.trechoSelecionado.addVoo(this.voo);
		}
		log.info("Salvando Voo #0", voo);
		this.voo = new Voo();
		return "/voos.xhtml";
	}

	public void editarVoo() {
		log.info("Editar Voo");
		this.voo = this.vooSelecionado;
	}

	public String apagarVoo() {
		log.info("Apagar Voo");
		return "voos.xhtml";
	}

	/**
	 * Getters
	 */
	public Voo getVoo() {
		return this.voo;
	}

	public Trecho getTrechoSelecionado() {
		return this.trechoSelecionado;
	}

	@Factory("voos")
	public void iniciarVoos() {
		this.voos = this.trechoSelecionado.getVoos();
	}

	/**
	 * Destroy
	 */
	@Remove
	@Destroy
	public void destruirEJB() {
		log.info("Chamando metodo de destruição do EJB");
	}

}
