package org.itsector.aeriscaelum.validation;

import java.util.Date;

import org.hibernate.validator.Validator;
import org.itsector.aeriscaelum.domain.Voo;

public class ChegadaMaiorQuePartidaValidator implements Validator<ChegadaMaiorQuePartida> {

	private ChegadaMaiorQuePartida anotacao;
	
	@Override
	public void initialize(ChegadaMaiorQuePartida anotacao) {
		this.anotacao = anotacao;
		
	}

	@Override
	public boolean isValid(Object entitdade) {
		Voo voo = (Voo) entitdade;
		Date chegada = voo.getDataChegada();
		Date partida = voo.getDataPartida();
		return chegada.after(partida);
	}

	
	
}
