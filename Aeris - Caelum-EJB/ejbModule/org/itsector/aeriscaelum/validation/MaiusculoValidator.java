package org.itsector.aeriscaelum.validation;

import org.hibernate.validator.Validator;

public class MaiusculoValidator implements Validator<Maiusculo> {

	private Maiusculo anotacao;
	
	@Override
	public void initialize(Maiusculo anotacao) {
		this.anotacao = anotacao;
		
	}

	@Override
	public boolean isValid(Object value) {
		String asString = value.toString();
		return asString.equals(asString.toUpperCase());
	}

}
